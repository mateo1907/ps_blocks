<?php

require_once 'CustomObjectModel.php';

class Block extends BlocksCustomObjectModel
{

  public $id_block;
  public $id_category;
  public $description;
  public $active;
  public $position;
  public $date_add;
  public $date_upd;


  public static $definition = [
    'table' => 'psblock',
    'primary' => 'id_block',
    'fields'    => [
        'id_block'           => ['type' => self::TYPE_INT, 'validate' => 'isInt'],
        'id_category'           => ['type' => self::TYPE_INT, 'db_type'=> 'int', 'validate' => 'isInt','required' => true],
        'description'         => ['type' => self::TYPE_HTML, 'db_type' => 'text', 'required' => true],

        'active'       => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'db_type' => 'int','required' => true],

        'date_add'      => [
            'type'     => self::TYPE_DATE,
            'validate' => 'isDate',
            'db_type'  => 'datetime',
        ],
        'date_upd'      => [
            'type'     => self::TYPE_DATE,
            'validate' => 'isDate',
            'db_type'  => 'datetime',
        ],
    ],
    'associations' => [
      'category' => ['type'=>self::HAS_ONE,'field'=>'id_category','object'=>'BlockCategory'],
    ],
  ];

  public static function getBlocksByCategory($id)
  {
    $sql = "SELECT * FROM " . _DB_PREFIX_ . self::$definition['table'] . " WHERE id_category = $id AND active = 1";

    return Db::getInstance()->ExecuteS($sql);
  }

}
