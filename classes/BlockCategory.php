<?php

require_once 'CustomObjectModel.php';

class BlockCategory extends BlocksCustomObjectModel
{
  public $id_category;
  public $show_title;
  public $name;
  public $active;
  public $hook;
  public $elementClass;
  public $offset;
  public $enableOffset;
  public $date_add;
  public $date_upd;


  public static $definition = [
    'table' => 'psblockcategory',
    'primary' => 'id_category',
    'fields'    => [
        'id_category'           => ['type' => self::TYPE_INT, 'validate' => 'isInt'],
        'show_title'  => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'db_type' => 'int','required' => true],
        'name'         => ['type' => self::TYPE_STRING, 'db_type' => 'varchar(255)','required' => true],
        'hook'         => ['type' => self::TYPE_STRING, 'db_type' => 'varchar(255)','required' => true],
        'elementClass'       => ['type' => self::TYPE_STRING, 'db_type' => 'varchar(255)','required' => true],
        'enableOffset' =>   ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'db_type' => 'int','required' => true],
        'offset'      => ['type' => self::TYPE_INT, 'db_type' => 'int', 'validate' => 'isInt','required' => true],
        'active'       => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'db_type' => 'int','required' => true],
        'date_add'      => [
            'type'     => self::TYPE_DATE,
            'validate' => 'isDate',
            'db_type'  => 'datetime',
        ],
        'date_upd'      => [
            'type'     => self::TYPE_DATE,
            'validate' => 'isDate',
            'db_type'  => 'datetime',
        ],
    ],
  ];

  public static function getBlockCategories()
  {
    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM ' . _DB_PREFIX_ . self::$definition['table'] . ' WHERE 1');
  }

  public static function getCategoriesByHookName($hook)
  {
    $sql = "SELECT * FROM " . _DB_PREFIX_ . self::$definition['table'] . " WHERE hook = '{$hook}' AND active = 1";
    return Db::getInstance()->ExecuteS($sql);
  }
}
