<?php

class BlocksCustomObjectModel extends ObjectModel
{
  public function createDb()
  {
    $definition = ObjectModel::getDefinition($this);

    $sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . $definition['table'] . ' (';
    $sql .= $definition['primary'] . ' INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,';

    foreach ($definition['fields'] as $name => $field) {
      if ($name === $definition['primary']) {
        continue;
      }

      $sql .= $name . ' ' . $field['db_type'];

      if (isset($field['required']) && $field['required'])
      {
          $sql .= ' NOT NULL';
      }

      if (isset($field['default']))
      {
          $sql .= ' DEFAULT "' . $field['default'] . '"';
      }

      $sql .= ',';


    }

    $sql = trim($sql,',');
    $sql .= ') CHARACTER SET utf8 COLLATE utf8_general_ci;';

    Db::getInstance()->execute($sql);


  }

  public function removeTable()
  {
    $definition = ObjectModel::getDefinition($this);

    $sql = 'DROP TABLE ' . _DB_PREFIX_ . $definition['table'];
    Db::getInstance()->execute($sql);
  }
}
