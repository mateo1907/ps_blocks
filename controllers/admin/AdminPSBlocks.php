<?php
require_once _PS_MODULE_DIR_ . 'ps_blocks/classes/Block.php';
require_once _PS_MODULE_DIR_ . 'ps_blocks/classes/BlockCategory.php';

class AdminPSBlocksController extends ModuleAdminController
{

  protected $image_dir;
  protected $categories;
  protected $obj;
  public function __construct()
  {
    $this->bootstrap = true;
    $this->table = 'psblock';
    $this->identifier = 'id_block';
    $this->className = 'Block';

    parent::__construct();

    $this->_join .= ' LEFT JOIN ' . _DB_PREFIX_ . 'psblockcategory b ON (b.id_category = a.id_category)';
    $this->_select .= ' b.name as categoryName';
    $this->categories = BlockCategory::getBlockCategories();
    $this->obj = $this->loadObject(true);
    $this->image_dir = '../modules/ps_blocks/img';

    $this->fields_list = [
      'id_block' => [
          'title' => $this->l('ID'),
          'type'=>'text',
          'align'=>'center',
          'class' => 'fixed-width-xs',
      ],
      'categoryName'     => [
          'title' => $this->l('Category'),
          'type'  => 'text',
      ],

      'image' => [
                                    'title' => $this->l('Image'),
                                    'image' => $this->image_dir,
                                    'orderby' => false,
                                    'search' => false,
                                    'width' => 200,
                                    'align' => 'center',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
      ],
      'active'   => [
          'title'  => $this->l('Status'),
          'active' => 'status',
          'align'  => 'text-center',
          'class'  => 'fixed-width-sm'
      ],
    
      'date_add' => [
          'title' => $this->l('Created'),
          'type'  => 'datetime',
      ],
      'date_upd' => [
          'title' => $this->l('Updated'),
          'type'  => 'datetime',
      ],
    ];

    $this->actions = ['edit', 'delete'];

    $this->bulk_actions = array(
        'delete' => array(
            'text'    => $this->l('Delete selected'),
            'icon'    => 'icon-trash',
            'confirm' => $this->l('Delete selected items?'),
        ),
    );


  }

  public function initContent()
  {
      parent::initContent();
  }

  public function renderForm()
  {
    $img_desc = '';

        if(Tools::getvalue('id_block') != '' && Tools::getvalue('id_block') != NULL){
             $img_desc .= '<br/><img style="height:auto;width:300px;clear:both;border:1px solid black;" alt="" src="'.__PS_BASE_URI__.'modules/ps_blocks/img/'.Tools::getvalue('id_block').'.jpg" /><br />';
        }
    $this->fields_form = [
      'legend' => [
        'title' => 'Block'
      ],

      'input' => [
        'active' => [
          'type' => 'switch',
                      'label' => $this->l('Enabled'),
                      'name' => 'active',
                      'class' => 't',
                      'is_bool' => true,
                      'values' => [
                        [
                          'id' => 'active_on',
                          'value' => 1,
                          'label' => $this->l('Enabled')
                        ],
                        [
                          'id' => 'active_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        ],
                      ],

        ],
        'name' => [
          'type'     => 'textarea',
          'label'    => $this->l('Description'),
          'name'     => 'description',
          'required' => true,
          'class' => 'rte',
          'autoload_rte' => true,
        ],
        'categoryName' => [
            'type' => 'select',
            'label' => $this->l('Category'),
            'name' => 'id_category',
            'options' => [
                'query' => $this->categories,
                'id' => 'id_category',
                'name' => 'name'
            ],
            'col' => '4',

        ],
        'image' => [
          'type' => 'file',
                    'label' => $this->l('Feature Image:'),
                    'name' => 'image',
                    'display_image' => false,
                    'desc' => $img_desc
        ],
      ],

      'submit' => [
          'title' => $this->l('Save'),
      ],
    ];

    return parent::renderForm();
  }
  public function postProcess()
  {

    if (Tools::isSubmit('submitAddpsblock')) {
      if (Tools::getValue('id_block') != '') {
        $block = new Block(Tools::getValue('id_block'));
        $block->id_category = Tools::getValue('id_category');
        $block->active = Tools::getValue('active');
        $block->description = pSQL(Tools::getValue('description',true),true);
        $block->update();
      } else {
        $block = new Block;
        $block->id_category = Tools::getValue('id_category');
        $block->active = Tools::getValue('active');
        $block->description = pSQL(Tools::getValue('description',true),true);
        $block->save();
      }

      $id = $block->id;
       $this->processImage($_FILES, $id);







      return true;
    }

    return parent::postProcess();


  }

  public function processImage($FILES,$id) {

            if (isset($FILES['image']) && isset($FILES['image']['tmp_name']) && !empty($FILES['image']['tmp_name'])) {
                if ($error = ImageManager::validateUpload($FILES['image'], 4000000))
                    return $this->displayError($this->l('Invalid image'));
                else {
                    $ext = substr($FILES['image']['name'], strrpos($FILES['image']['name'], '.') + 1);
                    $file_name = $id . '.' . $ext;


                    $path = _PS_MODULE_DIR_ .'ps_blocks/img/' . $file_name;


                    if (!move_uploaded_file($FILES['image']['tmp_name'], $path))
                        return $this->displayError($this->l('An error occurred while attempting to upload the file.'));

                }
            }
    }
}
