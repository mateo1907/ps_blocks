<?php
require_once _PS_MODULE_DIR_ . 'ps_blocks/classes/BlockCategory.php';

class AdminPSBlocksCategoriesController extends ModuleAdminController
{
  protected $hooks = [];
  protected $elementClass = [];
  protected $offset = [];
  public function __construct()
  {
    $this->bootstrap = true;
    $this->table = 'psblockcategory';
    $this->identifier = 'id_category';
    $this->className = 'BlockCategory';

    parent::__construct();
    $this->hooks = Module::getInstanceByName('ps_blocks')->hooks;
    for ($i=1; $i <= 12 ; $i++) {
      $this->elementClass[] = [
        'class' => 'col-md-' . $i
      ];

      $this->offset[] = ['i' => $i];

    }
    $this->fields_list = [
      'id_category' => [
        'title' => $this->l('ID'),
        'type'=>'text',
        'align'=>'center',
        'class' => 'fixed-width-xs',
      ],
      'name'     => [
          'title' => $this->l('Name'),
          'type'  => 'text',
      ],

      'hook' => [
        'title' => $this->l('Hook'),
        'type'  => 'text',
      ],
      'active'   => [
          'title'  => $this->l('Status'),
          'active' => 'status',
          'align'  => 'text-center',
          'class'  => 'fixed-width-sm'
      ],
      'date_add' => [
          'title' => $this->l('Created'),
          'type'  => 'datetime',
      ],
      'date_upd' => [
          'title' => $this->l('Updated'),
          'type'  => 'datetime',
      ],
    ];

    $this->actions = ['edit', 'delete'];

    $this->bulk_actions = array(
        'delete' => array(
            'text'    => $this->l('Delete selected'),
            'icon'    => 'icon-trash',
            'confirm' => $this->l('Delete selected items?'),
        ),
    );


  }


  public function renderForm()
  {

    $this->fields_form = [
      'legend' => [
        'title' => 'Category'
      ],
      'input' => [
        'show_title' => [
          'type' => 'switch',
                      'label' => $this->l('Show title'),
                      'name' => 'show_title',
                      'class' => 't',
                      'is_bool' => true,
                      'values' => [
                        [
                          'id' => 'show_on',
                          'value' => 1,
                          'label' => $this->l('Enabled')
                        ],
                        [
                          'id' => 'show_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        ],
                      ],

        ],
        'name' => [
          'type'     => 'text',
          'label'    => $this->l('Name'),
          'name'     => 'name',
          'required' => true,
        ],
        'hook' => [
            'type' => 'select',
            'label' => $this->l('Hooks'),
            'name' => 'hook',
            'options' => [
                'query' => $this->hooks,
                'id' => 'option',
                'name' => 'name'
            ],


        ],
        'class' => [
            'type' => 'select',
            'label' => $this->l('Class for each element'),
            'name' => 'elementClass',
            'options' => [
                'query' => $this->elementClass,
                'id' => 'class',
                'name' => 'class'
            ],


        ],
        'enableOffset' => [
                      'type' => 'switch',
                      'label' => $this->l('Enable offset'),
                      'name' => 'enableOffset',
                      'class' => 't',
                      'is_bool' => true,
                      'values' => [
                        [
                          'id' => 'offset_on',
                          'value' => 1,
                          'label' => $this->l('Enabled')
                        ],
                        [
                          'id' => 'offset_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        ],
                      ],

        ],
        'offset' => [
            'type' => 'select',
            'label' => $this->l('Offset to first element'),
            'name' => 'offset',
            'options' => [
                'query' => $this->offset,
                'id' => 'i',
                'name' => 'i'
            ],


        ],
        'active' => [
          'type' => 'switch',
                      'label' => $this->l('Enabled'),
                      'name' => 'active',
                      'class' => 't',
                      'is_bool' => true,
                      'values' => [
                        [
                          'id' => 'active_on',
                          'value' => 1,
                          'label' => $this->l('Enabled')
                        ],
                        [
                          'id' => 'active_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        ],
                      ],

        ],
      ],


      'submit' => [
          'title' => $this->l('Save'),
      ],
    ];

    return parent::renderForm();
  }
  public function initContent()
  {

      parent::initContent();
  }
}
