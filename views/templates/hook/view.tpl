
{foreach from=$obj item=o }
  <div class="row">
    {if $o.show_title}
      <h2>{$o.name}</h2>
    {/if}
    {foreach from=$o.blocks item=el key=k}

      <div class="{$o.elementClass} {if $k == 0 && $o.enableOffset} col-md-offset-{$o.offset} {/if} text-center">
        <img src="{$img_dir}/{$el.id_block}.jpg" alt="">
        <p>{$el.description}</p>
      </div>
    {/foreach} 
  </div>
{/foreach}
