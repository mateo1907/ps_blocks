<?php
if (!defined('_PS_VERSION_')) {
  exit;
}

class PS_Blocks extends Module
{

  public $models = ['BlockCategory','Block'];
  protected $image_dir;
  public $hooks = [
    [
      'option' => 'displayHome',
      'name' => 'displayHome',
    ],
    [
      'option' => 'displayHomeAfter',
      'name'=> 'displayHomeAfter',
    ],
    [
      'option' => 'displayCustomBlock',
      'name'=> 'displayCustomBlock',
    ],

  ];

  protected $tabs = [
    [
      'name' => 'Custom Blocks',
      'className' => 'AdminPSBlocks',
      'active' => 1,
      'childs' => [
        [
          'active' => 1,
          'name' => 'Blocks',
          'className' => 'AdminPSBlocks'
        ],
        [
          'active' => 1,
          'name' => 'Categories',
          'className' => 'AdminPSBlocksCategories'
        ]
      ],
    ],
  ];

  public function __construct()
  {
    $this->name = 'ps_blocks';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'MP';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('Prestashop Custom Blocks');
    $this->description = $this->l('You can create blocks on your page');

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    $this->image_dir = _PS_BASE_URL_ . '/modules/ps_blocks/img';

  }

  public function install()
  {
    foreach ($this->models as $model) {
      require 'classes/'. $model .'.php';
      $instance = new $model();

      $instance->createDb();

    }

    if (!parent::install() || !$this->registerHook('Home') || !$this->registerHook('displayHomeAfter') || !$this->registerHook('CustomBlock') ) {
      return false;
    }
    $this->addTab($this->tabs);
    return true;
  }

  public function uninstall(){
    foreach ($this->models as $model) {
      require 'classes/'. $model .'.php';
      $instance = new $model();

      // $instance->removeTable();

    }
      $this->removeTab($this->tabs);
      return parent::uninstall();
  }

  public function addTab(
      $tabs,
      $id_parent = 0
  )
  {
      foreach ($tabs as $tab)
      {
          $tabModel             = new Tab();
          $tabModel->module     = $this->name;
          $tabModel->active     = $tab['active'];
          $tabModel->class_name = $tab['className'];
          $tabModel->id_parent  = $id_parent;

          //tab text in each language
          foreach (Language::getLanguages(true) as $lang)
          {
              $tabModel->name[$lang['id_lang']] = $tab['name'];
          }

          $tabModel->add();

          //submenus of the tab
          if (isset($tab['childs']) && is_array($tab['childs']))
          {
              $this->addTab($tab['childs'], Tab::getIdFromClassName($tab['className']));
          }
      }
      return true;
  }

  public function removeTab($tabs)
  {
      foreach ($tabs as $tab)
      {
          $id_tab = (int) Tab::getIdFromClassName($tab["className"]);
          if ($id_tab)
          {
              $tabModel = new Tab($id_tab);
              $tabModel->delete();
          }

          if (isset($tab["childs"]) && is_array($tab["childs"]))
          {
              $this->removeTab($tab["childs"]);
          }
      }

      return true;
  }

  public function hookHome($params)
  {
    require_once 'classes/Block.php';
    require_once 'classes/BlockCategory.php';

    if (!Cache::isStored('psblocks')) {
      $categories = BlockCategory::getCategoriesByHookName('displayHome');
      $i=0;
      foreach ($categories as $c) {
        $blocks = Block::getBlocksByCategory($c['id_category']);
        $categories[$i]['blocks'] = $blocks;
        $i++;
      }

      Cache::store('psblocks',$categories);
    }

    $this->context->smarty->assign([
      'obj' => Cache::retrieve('psblocks'),
      'img_dir' => $this->image_dir
    ]);

    return $this->display(__FILE__,'view.tpl');
  }

  public function hookHomeAfter($params)
  {
    require_once 'classes/Block.php';
    require_once 'classes/BlockCategory.php';

    if (!Cache::isStored('psblocks')) {
      $categories = BlockCategory::getCategoriesByHookName('displayHomeAfter');
      $i=0;
      foreach ($categories as $c) {
        $blocks = Block::getBlocksByCategory($c['id_category']);
        $categories[$i]['blocks'] = $blocks;
        $i++;
      }

      Cache::store('psblocks',$categories);
    }

    $this->context->smarty->assign([
      'obj' => Cache::retrieve('psblocks'),
      'img_dir' => $this->image_dir
    ]);

    return $this->display(__FILE__,'view.tpl');
  }

  public function hookdisplayCustomBlock($params)
  {
    require_once 'classes/Block.php';
    require_once 'classes/BlockCategory.php';

    if (!Cache::isStored('psblocks')) {
      $categories = BlockCategory::getCategoriesByHookName('displayCustomBlock');
      $i=0;
      foreach ($categories as $c) {
        $blocks = Block::getBlocksByCategory($c['id_category']);
        $categories[$i]['blocks'] = $blocks;
        $i++;
      }

      Cache::store('psblocks',$categories);
    }

    $this->context->smarty->assign([
      'obj' => Cache::retrieve('psblocks'),
      'img_dir' => $this->image_dir
    ]);

    return $this->display(__FILE__,'view.tpl');
  }


}
